export function focusSelector(done, selector) {
  return this.evaluate_now(function(selector) {
    document.querySelector('#ctl00_secframe').contentDocument.querySelector(selector).focus();
  }, done.bind(this), selector);
};

export function blurSelector(done, selector) {
  return this.evaluate_now(function(selector) {
    document.querySelector('#ctl00_secframe').contentDocument.querySelector(selector).blur();
  }, done.bind(this), selector);
};
