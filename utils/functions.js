import credentials from '../credentials';
let passwordPositions = ["1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th","11th","12th","13th","14th","15th"];

export function getPinCode(labelText){
  for(let i = 0; i < credentials.pin.length; i++){
    let re = new RegExp(passwordPositions[i], "g");
    if(labelText.match(re)){
      return credentials.pin[i];
    }
  }
}
export function getPasswordCharacter(labelText){
  for(let i = 0; i < credentials.password.length; i++){
    let re = new RegExp("\\b" + passwordPositions[i], "g");
    if(labelText.match(re)){
      return credentials.password[i];
    }
  }
}
