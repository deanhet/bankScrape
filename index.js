import {rbs} from './rbs';
import {amex} from './amex';
import fs from 'fs';

(async function startScrape(){
  try{
    let balances = {};
    let rbsBalance = await rbs();
    let amexBalance = await amex();
    balances.RBS = (rbsBalance.replace("�","").replace(",","") - 100).toFixed(2)
    balances.Amex = amexBalance.replace("�","").replace(",","")
    let d = new Date();
    balances.lastUpdated = d.getTime();
    fs.writeFile("/tmp/bankscrape/balances.json", JSON.stringify(balances), function(err){
      if(err){
        throw new Error(err);
      } else {
        console.log("Successfully written to json file");
        process.exit(0);
      }
    })
  } catch(error){
    console.log(error);
    process.exit(1);
  }
})();
