import Nightmare from 'nightmare';
import {getPinCode, getPasswordCharacter} from './utils/functions';
import {focusSelector, blurSelector} from './utils/nightmare';
import credentials from './credentials';

Nightmare.action('customType', function(){
  let selector = arguments[0], text, done;
  if(arguments.length == 2) {
    done = arguments[1];
  } else {
    text = arguments[1];
    done = arguments[2];
  }

  let self = this;

  focusSelector.bind(this)(function() {
    let blurDone = blurSelector.bind(this, done, selector);
    if ((text || '') == '') {
      this.evaluate_now(function(selector) {
        document.querySelector('#ctl00_secframe').contentDocument.querySelector(selector).value = '';
      }, blurDone, selector);
    } else {
      self.child.call('type', text, blurDone);
    }
  }, selector);
})

Nightmare.action('customClick', function(selector, done) {
  this.evaluate_now(function (selector) {
    document.activeElement.blur();
    var element = document.querySelector('#ctl00_secframe').contentDocument.querySelector(selector);
    if (!element) {
      throw new Error('Unable to find element by selector: ' + selector);
    }
    var event = document.createEvent('MouseEvent');
    event.initEvent('click', true, true);
    element.dispatchEvent(event);
  }, done, selector);
});

let nightmare = Nightmare({
  show: false,
  openDevTools: false
})

export async function rbs(){
  return new Promise(function(resolve, reject){
    nightmare
    .useragent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2478.0 Safari/537.36')
    .goto('http://rbsdigital.com')
    .customType('input#ctl00_mainContent_LI5TABA_DBID_edit', credentials.rbsCustomerNumber)
    .customClick('#ctl00_mainContent_LI5TABA_LI5-LBA_button_button')
    .wait(7000)
    .evaluate(function(){
      return{
        inputOne: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[0].textContent,
        inputTwo: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[1].textContent,
        inputThree: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[2].textContent,
        inputFour: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[3].textContent,
        inputFive: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[4].textContent,
        inputSix: document.querySelector('#ctl00_secframe').contentDocument.querySelectorAll('label')[5].textContent
      }
    })
    .then(function(inputs){
      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPEA_edit', getPinCode(inputs.inputOne));
      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPEB_edit', getPinCode(inputs.inputTwo));
      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPEC_edit', getPinCode(inputs.inputThree));

      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPED_edit', getPasswordCharacter(inputs.inputFour));
      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPEE_edit', getPasswordCharacter(inputs.inputFive));
      nightmare.customType('#ctl00_mainContent_Tab1_LI6PPEF_edit', getPasswordCharacter(inputs.inputSix));
      nightmare.customClick('#ctl00_mainContent_Tab1_next_text_button_button')
      nightmare.wait(5000)
      return nightmare.evaluate(function(){
        return document.querySelector('#ctl00_secframe').contentDocument.querySelector('#Account_8E29C25079AA7F816CC37D4B5A073F2D25538BEE > td:nth-child(5)').textContent
      })
    })
    .then(function(text){
      nightmare.end().then();
      resolve(text);
    })
    .catch(function (error) {
      nightmare.screenshot('/tmp/bankscrape/failure.png')
      nightmare.end().then();
      reject(error);
    });
  })
}
