import Nightmare from 'nightmare';
import credentials from './credentials';
import {getPinCode, getPasswordCharacter} from './utils/functions';
const nightmare = Nightmare({
    show: false,
    openDevTools: false
});

export async function amex(){
  return new Promise(function(resolve, reject){
    nightmare
    .useragent('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2478.0 Safari/537.36')
    .goto('https://global.americanexpress.com/myca/logon/emea/action/LogonHandler?request_type=LogonHandler&Face=en_GB&inav=gb_utility_login')
    .type('#lilo_userName', credentials.amexUsername)
    .type('input[name=Password]', credentials.amexPassword)
    .click('#lilo_formSubmit')
    .wait('#balance-summary-table')
    .evaluate(function(){
      return document.querySelector('#balance-summary-table > tbody > tr.available-credit > td.total > span').textContent
    })
    .then(function(text){
      nightmare.end().then();
      resolve(text);
    })
    .catch(function (error) {
      reject(error);
    });
  })
}
